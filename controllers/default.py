# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

# ---- example index page ----
def index():
    response.flash = T("Hello World")
    return dict(message=T('Welcome to web2py!'))


def a():
    response.headers["Access-Control-Allow-Origin"] = 'http://minasgerais.com.br,http://www.minasgerais.com.br'
    response.headers['Access-Control-Max-Age'] = 86400
    response.headers['Access-Control-Allow-Headers'] = '*'
    response.headers['Access-Control-Allow-Methods'] = '*'

    import os
    import sys
    #print(request.env)
    #print(len(request.post_vars))


    def get_cookie():

        cookie = None
        if request.post_vars.c:

            c = request.post_vars.c

        else:
            c = request.env['HTTP_COOKIE']
            #print(c)
        r = None
        if c:
            r = c.split(';')
        #print(c,r)
        if r:
            for row in r:
                #print(row)
                if '_ga=' in row:
                    cookie = row.split('=')[1]
                    #print(cookie)
                    break

        return cookie

    if request.post_vars:

        p = request.post_vars
        #print(p)
        #print(request.env)
        if sys.version_info[0] == 3:
            from urllib.parse import urlparse

        else:
            from urlparse import urlparse

        parsed = urlparse(request.post_vars.url)
        cookie = get_cookie()

        dominios = ['www.minasgerais.com.br', 'minasgerais.com.br','planodegastronomia.mg.gov.br']




        dispositivo, sistema_operacional, navegador, bot =  'PC', None, None, None
        user_agent = request.env['HTTP_USER_AGENT']
        bots = ['Googlebot', 'Yandexbot', 'AhrefsBot']

        ip = request.env.remote_addr

        for b in bots:
            if b in user_agent:
                bot = True


        if parsed.netloc in dominios and not bot:
            sos = ['Windows', 'Android', 'Linux', 'iPhone OS']
            navs = {'Firefox': 'Firefox', 'Chrome': 'Chrome', 'Trident': 'Internet Explorer',
                    'Safari':'Safari'}

            mobile = ['Android', 'iPhone']
            for d in mobile:
                if d in user_agent:
                    dispositivo = 'celular'
                    break
            for so in sos:
                if so in user_agent:
                    sistema_operacional = so
                    break
            for nav in navs.keys():
                if nav in user_agent:
                    navegador = navs[nav]
                    break



            #ip = request.env.remote_addr
            #print(ip,parsed)

            import geoip2.database

            file = os.path.join(request.folder, 'private/geolite2-city.mmdb')

            reader = geoip2.database.Reader(file)

            pais, estado, cidade, latitude, longitude = None, None, None, None, None
            try:
                r = reader.city(ip)
                pais = r.country.names['pt-BR']
                estado = r.subdivisions.most_specific.name
                cidade = r.city.name
                latitude = r.location.latitude
                longitude = r.location.longitude
            except:
                #print('erro')
                pass


            if ip[:3] == '10.':
                pais = 'Brasil'
                estado = 'Minas Gerais'
                cidade = 'Belo Horizonte'
                latitude = '-19.9017'
                longitude = '-43.9642'

            evento = None
            if parsed.path[4:12] == 'eventos/':
                #print(parsed.path)
                evento = parsed.path[12:]

            b = ''
            if parsed.query:
                b = parsed.query.split('=')[1].replace('+',' ')

                if sys.version_info[0] == 3:
                    from urllib.parse import unquote
                    b = unquote(b)
                else:
                    import urllib
                    b = urllib.unquote(b).decode('utf-8')


            id = db.pageview.insert(
                sitedomain=parsed.netloc,
                url=parsed.path,
                busca=b,
                slug_evento=evento,
                title=p.t or None,
                ip=ip,
                referrer=p.ref or None,
                usuario=cookie,
                http_user_agent=user_agent,
                http_accept_language=request.env['HTTP_ACCEPT_LANGUAGE'],
                pais=pais,
                estado=estado,
                cidade=cidade,
                longitude=longitude,
                latitude=latitude,
                dispositivo = dispositivo,
                sistema_operacional = sistema_operacional,
                navegador=navegador,
            )
            #print(db._lastsql)
            db.commit()
            #print(id)

    #gera uma imagem de 1 pixel para retorno

    return response.json('ok')


def b():
    response.headers["Access-Control-Allow-Origin"] = 'http://www.minasgerais.com.br'
    response.headers['Access-Control-Max-Age'] = 86400
    response.headers['Access-Control-Allow-Headers'] = '*'
    response.headers['Access-Control-Allow-Methods'] = '*'

    import os
    import sys
    #print(request.env)
    #print(len(request.get_vars))




    if request.get_vars:

        p = request.get_vars
        #print(p)
        #print(request.env)

        if sys.version_info[0] == 3:
            from urllib.parse import urlparse1
        else:
            from urlparse import urlparse

        parsed = urlparse(p.url.replace('|','.'))
        #cookie = p.c

        dominios = ['www.minasgerais.com.br', 'minasgerais.com.br', 'planodegastronomia.mg.gov.br']


        dispositivo, sistema_operacional, navegador, bot =  'PC', None, None, None
        user_agent = request.env['HTTP_USER_AGENT']
        bots = ['Googlebot', 'Yandexbot', 'AhrefsBot']

        ip = request.env.remote_addr or None
        #ip = "201.17.211.62"
        print(ip)
        for b in bots:
            if b in user_agent:
                bot = True


        if parsed.netloc in dominios and  not bot:
            sos = ['Windows', 'Android', 'Linux', 'iPhone OS']
            navs = {'Firefox': 'Firefox', 'Chrome': 'Chrome', 'Trident': 'Internet Explorer',
                    'Safari':'Safari'}

            mobile = ['Android', 'iPhone']
            for d in mobile:
                if d in user_agent:
                    dispositivo = 'celular'
                    break
            for so in sos:
                if so in user_agent:
                    sistema_operacional = so
                    break
            for nav in navs.keys():
                if nav in user_agent:
                    navegador = navs[nav]



            #ip = request.env.remote_addr
            #print(ip,parsed)

            import geoip2.database

            file = os.path.join(request.folder, 'private/geolite2-city.mmdb')

            reader = geoip2.database.Reader(file)

            pais, estado, cidade, latitude, longitude = None, None, None, None, None
            try:
                r = reader.city(ip)
                pais = r.country.names['pt-BR']
                estado = r.subdivisions.most_specific.name
                cidade = r.city.name
                latitude = r.location.latitude
                longitude = r.location.longitude
            except:
                #print('erro')
                pass

            if ip[:3] == '10.':
                pais = 'Brasil'
                estado = 'Minas Gerais'
                cidade = 'Belo Horizonte'
                latitude = '-19.9017'
                longitude = '-43.9642'

            evento = None
            split_url = parsed.path.split('/')

            if len(split_url)>= 3 and  (split_url[2] == 'eventos' or split_url[2]=='carnaval-2019'):
                #print(p.url)
                lu = len(split_url)
                if lu == 4:
                    evento = split_url[3]
                elif lu == 5:
                    evento = split_url[4]
            #print(evento, parsed.path)
            if p.ref:
                ref = p.ref.replace('|','.')
            else:
                ref = ''


            b=''
            if parsed.query and parsed.path == "/pt/busca":
                s = parsed.query.split('=')
                if len(s)>1:
                    b = parsed.query.split('=')[1].replace('+',' ')
                    if sys.version_info[0] == 2:
                        import urllib
                        b = urllib.unquote(b).decode('utf-8')
                    else:
                        from urllib.parse import unquote
                        b = unquote(b)
            #print(ref)
            id = db.pageview.insert(
                sitedomain=parsed.netloc,
                url=parsed.path,
                busca = b,
                slug_evento=evento,
                title=p.t or None,
                ip=ip,
                referrer=ref,
                usuario=p.c or None,
                http_user_agent=user_agent,
                http_accept_language=request.env['HTTP_ACCEPT_LANGUAGE'],
                pais=pais,
                estado=estado,
                cidade=cidade,
                longitude=longitude,
                latitude=latitude,
                dispositivo = dispositivo,
                sistema_operacional = sistema_operacional,
                navegador=navegador,
            )
            #print(db._lastsql)
            db.commit()
            return  response.json('registrado com id {}'.format(id))
    else:
      return  response.json('sem get_vars')

    return response.json('ok')

def ajs():

    js = """(function(){
    var d=document,i=new Image,e=encodeURIComponent;
    i.src='%s?url='+e(d.location.href)+'&ref='+e(d.referrer)+'&t='+e(d.title);
    })()""".replace('\n', '') % (URL(scheme=True, host=True, f='a'))


    js = """<textarea cols=160 rows=50>
<script>
{}
</script>
</textarea>""".format(js)
    return js

# ---- API (example) -----
@auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET': raise HTTP(403)
    return response.json({'status':'success', 'email':auth.user.email})

# ---- Smart Grid (example) -----
@auth.requires_membership('admin') # can only be accessed by members of admin groupd
def grid():
    response.view = 'generic.html' # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables: raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[tablename], deletable=False, editable=False)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----
def wiki():
    auth.wikimenu() # add the wiki to the menu
    return auth.wiki() 

# ---- Action for login/register/etc (required for auth) -----
def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    return dict(form=auth())

# ---- action to server uploaded static content (required) ---
@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)
