#!/usr/bin/env python
# A fabric file to commit, push and update server


from fabric.api import *
from datetime import datetime

env.hosts = ['web2py@200.198.6.251']

def backup_db():
    run("cd /opt/web2py_apps/web2py/applications/analytics/private")
    run("sh backup.sh")

def backup_data():

    run("cd /opt/web2py_apps/web2py/applications/")
    run("7z u analytics.7z -r analytics/ -mmt")
def clean():
    import os
    path = os.path.dirname(os.path.abspath(__file__))
    #os.system('rm -rf {}/*'.format(os.path.join(path, 'static/tmp')))
    os.system('rm -rf {}/*'.format(os.path.join(path, 'sessions')))
    os.system('rm -rf {}/*'.format(os.path.join(path, 'errors')))
    os.system('rm -rf {}/*.table'.format(os.path.join(path, 'databases')))
    #os.system('rm -rf {}/*'.format(os.path.join(path, 'static/relatorio')))

def get():
    run("cd /opt/web2py_apps/web2py/applications/analytics")
    run("git stash")
    local("git pull")


def commit():
    try:
        now = datetime.now().isoformat()

        local("echo '{}' > VERSION.md".format(now))
        local("git add --all . && git commit -m '%s'" % now)
        print(now)
    except:
        print("Is not possible to commit")  # print(datetime.now().isoformat())


def push():
    local("git push")


def update():
    with cd("/opt/web2py_apps/web2py/applications/analytics"):
        run("git stash")
        run("git pull")
        # stop_servers()
        # start_servers()
#git config --global http.proxy http://m667566:Loi8uhg2@proxycamg.prodemge.gov.br:8080

def deploy():
    clean()
    commit()
    push()
    update()
    print(datetime.now().isoformat())




